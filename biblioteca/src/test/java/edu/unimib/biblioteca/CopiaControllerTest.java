package edu.unimib.biblioteca;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.unimib.biblioteca.model.Autore;
import edu.unimib.biblioteca.model.CasaEditrice;
import edu.unimib.biblioteca.model.ClienteNonTesserato;
import edu.unimib.biblioteca.model.Copia;
import edu.unimib.biblioteca.model.Libro;
import java.util.HashSet;
import java.util.Set;
import javax.transaction.Transactional;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CopiaControllerTest {
  @Autowired private MockMvc mvc;
  private static int copiaId;
  private static int libroId;
  private static int clienteId;
  private static boolean disponibile = false;
  private static Copia copia;
  private static String libroTitolo = "Harry Potter 12";
  private static String clienteCodiceFiscale = "ERSZRA30A21H501I";

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Test
  @Transactional
  public void startTests() throws Exception {
	  createCopiaAPI();
	  getAllCopieAPI();
	  getCopieBySearchAPI();
	  getCopiaAPI();
	  updateCopiaAPI();
	  searchByCodiceFiscaleClienteAPI();
	  searchByLibroTitoloAPI();
	  deleteCopiaAPI();
  }
  
  public void createCopiaAPI() throws Exception {
    Set<Autore> autori = new HashSet<Autore>();
    Autore autore = new Autore();
    autore.setNome("Joanne");
    autore.setCognome("Rowling");

    MvcResult mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/autori")
                    .content(asJsonString(autore))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    autore =
        new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), Autore.class);

    CasaEditrice casaEditrice = new CasaEditrice();
    casaEditrice.setNome("Adriano Salani Editore");

    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/caseEditrici")
                    .content(asJsonString(casaEditrice))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    casaEditrice =
        new ObjectMapper()
            .readValue(mvcResult.getResponse().getContentAsString(), CasaEditrice.class);

    autori.add(autore);
    Libro libro = new Libro();
    libro.setIsbn(9788831003384L);
    libro.setTitolo(libroTitolo);
    libro.setAnno(1997);
    libro.setCasaEditrice(casaEditrice);
    libro.setAutori(autori);

    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/libri")
                    .content(asJsonString(libro))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    libro = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), Libro.class);
    libroId = (int) libro.getId();

    ClienteNonTesserato clienteNonTesserato = new ClienteNonTesserato();
    clienteNonTesserato.setNome("Franceso");
    clienteNonTesserato.setCognome("Rossi");
    clienteNonTesserato.setCodiceFiscale(clienteCodiceFiscale);

    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/clientiNonTesserati")
                    .content(asJsonString(clienteNonTesserato))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    clienteNonTesserato =
        new ObjectMapper()
            .readValue(mvcResult.getResponse().getContentAsString(), ClienteNonTesserato.class);
    clienteId = (int) clienteNonTesserato.getId();

    copia = new Copia();
    copia.setCliente(clienteNonTesserato);
    copia.setLibro(libro);
    copia.setDisponibile(false);

    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/copie")
                    .content(asJsonString(copia))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();

    copia = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), Copia.class);
    copiaId = (int) copia.getId();
  }

  public void getAllCopieAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/copie").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].id").value(copiaId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].disponibile")
                .value(disponibile))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].libro.id").value(libroId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].cliente.id")
                .value(clienteId));
  }

  public void getCopieBySearchAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get(
                    "/copie?search=id:{copiaId},disponibile:{disponibile}", copiaId, disponibile)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].id").value(copiaId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].disponibile")
                .value(disponibile))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].libro.id").value(libroId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + copiaId + ")].cliente.id")
                .value(clienteId));
  }


  public void getCopiaAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get("/copie/{copiaId}", copiaId)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.libro.id").value(libroId))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cliente.id").value(clienteId))
        .andExpect(MockMvcResultMatchers.jsonPath("$.disponibile").value(disponibile));
  }

  public void updateCopiaAPI() throws Exception {
    disponibile = true;
    copia.setCliente(null);
    copia.setDisponibile(disponibile);

    mvc.perform(
            MockMvcRequestBuilders.post("/copie/{copiaId}", copiaId)
                .content(asJsonString(copia))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.cliente").value(IsNull.nullValue()))
        .andExpect(MockMvcResultMatchers.jsonPath("$.disponibile").value(disponibile));
  }

  public void searchByCodiceFiscaleClienteAPI() throws Exception {
	    mvc.perform(
	            MockMvcRequestBuilders.get("/copie/byCodiceFiscaleCliente/{codiceFiscaleCliente}", clienteCodiceFiscale)
	                .contentType(MediaType.APPLICATION_JSON)
	                .accept(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
  }

  public void searchByLibroTitoloAPI() throws Exception {
	    mvc.perform(
	            MockMvcRequestBuilders.get("/copie/byLibroTitolo/{libroTitolo}", libroTitolo)
	                .contentType(MediaType.APPLICATION_JSON)
	                .accept(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
  }

  public void deleteCopiaAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.delete("/copie/{copiaId}", copiaId))
        .andExpect(status().isOk());
  }
}
