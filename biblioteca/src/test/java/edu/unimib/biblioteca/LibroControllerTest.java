package edu.unimib.biblioteca;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.unimib.biblioteca.model.Autore;
import edu.unimib.biblioteca.model.CasaEditrice;
import edu.unimib.biblioteca.model.Libro;
import java.util.HashSet;
import java.util.Set;
import javax.transaction.Transactional;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext
public class LibroControllerTest {
  @Autowired private MockMvc mvc;
  private static int libroId;
  private static String titolo = "Harry Potter e la pietra filosofale";
  private static long isbn = 9788831003384L;
  private static int anno = 1997;
  private static Libro libro;
  private static String nomeCasaEditrice = "Adriano Editore";;
  private static String nomeAutore = "Joanne";

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Test
  @Transactional
  public void startTests() throws Exception {
	  createLibroAPI();
	  getAllLibriAPI();
	  getLibriBySearchAPI();
	  getLibroAPI();
	  updateLibroAPI();
	  searchByNomeCasaEditriceAPI();
	  searchByNomeAutoreAPI();
	  deleteLibroAPI();
  }

  public void createLibroAPI() throws Exception {
    Set<Autore> autori = new HashSet<Autore>();
    Autore autore = new Autore();
    autore.setNome(nomeAutore);
    autore.setCognome("Rowling");

    MvcResult mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/autori")
                    .content(asJsonString(autore))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    autore =
        new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), Autore.class);
   
    CasaEditrice casaEditrice = new CasaEditrice();
    casaEditrice.setNome(nomeCasaEditrice);

    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/caseEditrici")
                    .content(asJsonString(casaEditrice))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    casaEditrice =
        new ObjectMapper()
            .readValue(mvcResult.getResponse().getContentAsString(), CasaEditrice.class);

    autori.add(autore);
    libro = new Libro();
    libro.setIsbn(9788831003384L);
    libro.setTitolo("Harry Potter e la pietra filosofale");
    libro.setAnno(1997);
    libro.setCasaEditrice(casaEditrice);
    libro.setAutori(autori);

    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/libri")
                    .content(asJsonString(libro))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    libro = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), Libro.class);
    libroId = (int) libro.getId();
    libro.setId(libroId);
  }

  public void getAllLibriAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/libri").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].titolo").value(titolo))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].isbn").value(isbn))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].anno").value(anno));
  }

  public void getLibriBySearchAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get(
                    "/libri?search=id:{libroId},titolo:{titolo},isbn:{isbn},anno:{anno}",
                    libroId,
                    "Harry Potter*",
                    isbn,
                    anno)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].titolo").value(titolo))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].isbn").value(isbn))
        .andExpect(MockMvcResultMatchers.jsonPath("$.[0].anno").value(anno));
  }

  public void getLibroAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get("/libri/{libroId}", libroId)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.titolo").value(titolo))
        .andExpect(MockMvcResultMatchers.jsonPath("$.isbn").value(isbn))
        .andExpect(MockMvcResultMatchers.jsonPath("$.anno").value(anno));
  }

  public void updateLibroAPI() throws Exception {
    titolo = "Potter e la pietra filosofale";
    isbn = 978883100338L;
    anno = 2020;

    libro.setTitolo(titolo);
    libro.setIsbn(isbn);
    libro.setAnno(anno);

    mvc.perform(
            MockMvcRequestBuilders.post("/libri/{libroId}", libroId)
                .content(asJsonString(libro))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.titolo").value(titolo))
        .andExpect(MockMvcResultMatchers.jsonPath("$.isbn").value(isbn))
        .andExpect(MockMvcResultMatchers.jsonPath("$.anno").value(anno));
  }

  public void searchByNomeCasaEditriceAPI() throws Exception {
	    mvc.perform(
	            MockMvcRequestBuilders.get("/libri/byNomeCasaEditrice/{nomeCasaEditrice}", nomeCasaEditrice)
	                .contentType(MediaType.APPLICATION_JSON)
	                .accept(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
  }

  public void searchByNomeAutoreAPI() throws Exception {
	    mvc.perform(
	            MockMvcRequestBuilders.get("/libri/byNomeAutore/{nomeAutore}", nomeAutore)
	                .contentType(MediaType.APPLICATION_JSON)
	                .accept(MediaType.APPLICATION_JSON))
	        .andExpect(status().isOk());
  }

  public void deleteLibroAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.delete("/libri/{libroId}", libroId))
        .andExpect(status().isOk());
  }
}
