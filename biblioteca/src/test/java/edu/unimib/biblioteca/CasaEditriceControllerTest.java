package edu.unimib.biblioteca;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import javax.transaction.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.unimib.biblioteca.model.CasaEditrice;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CasaEditriceControllerTest {
  @Autowired private MockMvc mvc;
  private static int casaEditriceId;
  private static String casaEditriceNome = "Adriano Salani Editore";

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  
  @Test
  @Transactional
  public void startTests() throws Exception {
	  createCasaEditriceAPI();
	  getAllCaseEdtriciAPI();
	  getCaseEdtriciBySearchAPI();
	  getCasaEdtriceAPI();
	  updateCasaEditriceAPI();
	  deleteCasaEditriceAPI();	  
  }

  public void createCasaEditriceAPI() throws Exception {
    CasaEditrice casaEditrice = new CasaEditrice();
    casaEditrice.setNome(casaEditriceNome);

    MvcResult mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/caseEditrici")
                    .content(asJsonString(casaEditrice))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    casaEditrice =
        new ObjectMapper()
            .readValue(mvcResult.getResponse().getContentAsString(), CasaEditrice.class);

    casaEditriceId = (int) casaEditrice.getId();
  }

  public void getAllCaseEdtriciAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/caseEditrici").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + casaEditriceId + ")].nome")
                .value(casaEditriceNome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + casaEditriceId + ")].id")
                .value(casaEditriceId));
  }

  public void getCaseEdtriciBySearchAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get(
                    "/caseEditrici?search=nome:{casaEditriceNome},id:{casaEditriceId}",
                    casaEditriceNome,
                    casaEditriceId)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + casaEditriceId + ")].nome")
                .value(casaEditriceNome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + casaEditriceId + ")].id")
                .value(casaEditriceId));
  }

  public void getCasaEdtriceAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get("/caseEditrici/{casaEditriciId}", casaEditriceId)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(casaEditriceNome));
  }

  public void updateCasaEditriceAPI() throws Exception {
    casaEditriceNome = "Salani Editore";
    CasaEditrice casaEditrice = new CasaEditrice();
    casaEditrice.setNome(casaEditriceNome);

    mvc.perform(
            MockMvcRequestBuilders.post("/caseEditrici/{casaEditriciId}", casaEditriceId)
                .content(asJsonString(casaEditrice))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(casaEditriceNome));
  }


  public void deleteCasaEditriceAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.delete("/caseEditrici/{casaEditriciId}", casaEditriceId))
        .andExpect(status().isOk());
  }
}
