package edu.unimib.biblioteca;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import javax.transaction.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.unimib.biblioteca.model.Autore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class AutoreControllerTest {
  @Autowired private MockMvc mvc;

  private static int autoriId;
  private static String autoreNome = "Joanne";
  private static String autoreCognome = "Rowling";

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  
  @Test
  @Transactional
  public void startTests() throws Exception {
	  createAutoreAPI();
	  getAllAutoriAPI();
	  getAutoriBySearchAPI();
	  getAutoreAPI();
	  updateAutoreAPI();
	  deleteAutoreAPI();	  
  }

  public void createAutoreAPI() throws Exception {
    Autore autore = new Autore();
    autore.setNome(autoreNome);
    autore.setCognome(autoreCognome);

    MvcResult mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/autori")
                    .content(asJsonString(autore))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    autore =
        new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), Autore.class);

    autoriId = (int) autore.getId();
  }

  public void getAllAutoriAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/autori").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + autoriId + ")].id").value(autoriId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + autoriId + ")].nome").value(autoreNome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + autoriId + ")].cognome")
                .value(autoreCognome));
  }

  public void getAutoriBySearchAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get(
                    "/autori?search=nome:{autoreNome},nome!Mario,cognome:{autoreCognome},id>10",
                    autoreNome,
                    autoreCognome)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + autoriId + ")].id").value(autoriId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + autoriId + ")].nome").value(autoreNome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + autoriId + ")].cognome")
                .value(autoreCognome));
  }

  public void getAutoreAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get("/autori/{autoriId}", autoriId)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(autoreNome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cognome").value(autoreCognome));
  }

  public void updateAutoreAPI() throws Exception {
    autoreNome = "Mario";
    autoreCognome = "Rossi";
    Autore autore = new Autore();
    autore.setNome(autoreNome);
    autore.setCognome(autoreCognome);

    mvc.perform(
            MockMvcRequestBuilders.post("/autori/{autoriId}", autoriId)
                .content(asJsonString(autore))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(autoreNome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cognome").value(autoreCognome));
  }

  public void deleteAutoreAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.delete("/autori/{autoriId}", autoriId))
        .andExpect(status().isOk());
  }
}
