package edu.unimib.biblioteca;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import javax.transaction.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.unimib.biblioteca.model.ClienteTesserato;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ClienteTesseratoControllerTest {
  @Autowired private MockMvc mvc;
  private static int clienteTesseratoId;
  private static String nome = "Mario";
  private static String cognome = "Rossi";
  private static String codiceFiscale = "RSSMRA30A01H501I";
  private static int idTessera = 12345;

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  
  @Test
  @Transactional
  public void startTests() throws Exception {
	  createClienteTesseratoAPI();
	  getAllClientiTesseratiAPI();
	  getClientiTesseratiBySearchAPI();
	  getClienteTesseratoAPI();
	  updateClienteTesseratoAPI();
	  deleteClienteTesseratoAPI();	  
  }
  
  public void createClienteTesseratoAPI() throws Exception {
    ClienteTesserato clienteTesserato = new ClienteTesserato();
    clienteTesserato.setNome(nome);
    clienteTesserato.setCognome(cognome);
    clienteTesserato.setCodiceFiscale(codiceFiscale);
    clienteTesserato.setIdTessera((long) idTessera);

    MvcResult mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/clientiTesserati")
                    .content(asJsonString(clienteTesserato))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    clienteTesserato =
        new ObjectMapper()
            .readValue(mvcResult.getResponse().getContentAsString(), ClienteTesserato.class);
    clienteTesseratoId = (int) clienteTesserato.getId();
  }

  public void getAllClientiTesseratiAPI() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/clientiTesserati").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].nome")
                .value(nome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].cognome")
                .value(cognome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].codiceFiscale")
                .value(codiceFiscale))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].idTessera")
                .value(idTessera));
  }

  public void getClientiTesseratiBySearchAPI() throws Exception {

    mvc.perform(
            MockMvcRequestBuilders.get(
                    "/clientiTesserati?search=nome:{nome},nome!Luigi,cognome:{cognome},id>10,codiceFiscale:RS*,idTessera:{idTessera}",
                    nome,
                    cognome,
                    idTessera)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].nome")
                .value(nome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].cognome")
                .value(cognome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].codiceFiscale")
                .value(codiceFiscale))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteTesseratoId + ")].idTessera")
                .value(idTessera));
  }

  public void getClienteTesseratoAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get("/clientiTesserati/{clienteTesseratoId}", clienteTesseratoId)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(nome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cognome").value(cognome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.codiceFiscale").value(codiceFiscale))
        .andExpect(MockMvcResultMatchers.jsonPath("$.idTessera").value(idTessera));
  }

  public void updateClienteTesseratoAPI() throws Exception {
    nome = "Luigi";
    cognome = "Verdi";
    codiceFiscale = "VSSMRA30A01H501I";
    idTessera = 12356;

    ClienteTesserato clienteTesserato = new ClienteTesserato();
    clienteTesserato.setNome(nome);
    clienteTesserato.setCognome(cognome);
    clienteTesserato.setCodiceFiscale(codiceFiscale);
    clienteTesserato.setIdTessera((long) idTessera);

    mvc.perform(
            MockMvcRequestBuilders.post(
                    "/clientiTesserati/{clienteTesseratoId}", clienteTesseratoId)
                .content(asJsonString(clienteTesserato))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(nome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cognome").value(cognome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.codiceFiscale").value(codiceFiscale))
        .andExpect(MockMvcResultMatchers.jsonPath("$.idTessera").value(idTessera));
  }

  public void deleteClienteTesseratoAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.delete(
                "/clientiTesserati/{clienteTesseratoId}", clienteTesseratoId))
        .andExpect(status().isOk());
  }
}
