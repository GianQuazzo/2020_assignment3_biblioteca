package edu.unimib.biblioteca;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import javax.transaction.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.unimib.biblioteca.model.ClienteNonTesserato;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ClienteNonTesseratoControllerTest {
  @Autowired private MockMvc mvc;
  private static int clienteNonTesseratoId;
  private static String nome = "Mario";
  private static String cognome = "Rossi";
  private static String codiceFiscale = "RSSMRA30A01H501I";

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  
  @Test
  @Transactional
  public void startTests() throws Exception {
	  createClienteNonTesseratoAPI();
	  getAllClientiNonTesseratiAPI();
	  getClientiNonTesseratiBySearchAPI();
	  getClienteNonTesseratoAPI();
	  updateClienteNonTesseratoAPI();
	  deleteClienteNonTesseratoAPI();	  
  }

  public void createClienteNonTesseratoAPI() throws Exception {
    ClienteNonTesserato clienteNonTesserato = new ClienteNonTesserato();
    clienteNonTesserato.setNome(nome);
    clienteNonTesserato.setCognome(cognome);
    clienteNonTesserato.setCodiceFiscale(codiceFiscale);

    MvcResult mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.post("/clientiNonTesserati")
                    .content(asJsonString(clienteNonTesserato))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andReturn();

    clienteNonTesserato =
        new ObjectMapper()
            .readValue(mvcResult.getResponse().getContentAsString(), ClienteNonTesserato.class);
    clienteNonTesseratoId = (int) clienteNonTesserato.getId();
  }

  public void getAllClientiNonTesseratiAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get("/clientiNonTesserati").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteNonTesseratoId + ")].id")
                .value(clienteNonTesseratoId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteNonTesseratoId + ")].nome")
                .value(nome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteNonTesseratoId + ")].cognome")
                .value(cognome))
        .andExpect(
            MockMvcResultMatchers.jsonPath(
                    "$[?(@.id==" + clienteNonTesseratoId + ")].codiceFiscale")
                .value(codiceFiscale));
  }

  public void getClientiNonTesseratiBySearchAPI() throws Exception {

    mvc.perform(
            MockMvcRequestBuilders.get(
                    "/clientiNonTesserati?search=nome:{nome},nome!Lugi,cognome:{Rossi},codiceFiscale:RS*,id>10",
                    nome,
                    cognome)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteNonTesseratoId + ")].id")
                .value(clienteNonTesseratoId))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteNonTesseratoId + ")].nome")
                .value(nome))
        .andExpect(
            MockMvcResultMatchers.jsonPath("$[?(@.id==" + clienteNonTesseratoId + ")].cognome")
                .value(cognome))
        .andExpect(
            MockMvcResultMatchers.jsonPath(
                    "$[?(@.id==" + clienteNonTesseratoId + ")].codiceFiscale")
                .value(codiceFiscale));
  }

  public void getClienteNonTesseratoAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.get(
                    "/clientiNonTesserati/{clienteNonTesseratoId}", clienteNonTesseratoId)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(nome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cognome").value(cognome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.codiceFiscale").value(codiceFiscale));
  }

  public void updateClienteNonTesseratoAPI() throws Exception {
    nome = "Mariapaola";
    cognome = "Maggioni";
    codiceFiscale = "MGGMPL07B63A944Q";
    ClienteNonTesserato ClienteNonTesserato = new ClienteNonTesserato();
    ClienteNonTesserato.setNome(nome);
    ClienteNonTesserato.setCognome(cognome);
    ClienteNonTesserato.setCodiceFiscale(codiceFiscale);

    mvc.perform(
            MockMvcRequestBuilders.post(
                    "/clientiNonTesserati/{clienteNonTesseratoId}", clienteNonTesseratoId)
                .content(asJsonString(ClienteNonTesserato))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(nome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.cognome").value(cognome))
        .andExpect(MockMvcResultMatchers.jsonPath("$.codiceFiscale").value(codiceFiscale));
  }

  public void deleteClienteNonTesseratoAPI() throws Exception {
    mvc.perform(
            MockMvcRequestBuilders.delete(
                "/clientiNonTesserati/{clienteNonTesseratoId}", clienteNonTesseratoId))
        .andExpect(status().isOk());
  }
}
