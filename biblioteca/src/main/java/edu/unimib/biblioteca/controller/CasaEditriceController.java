package edu.unimib.biblioteca.controller;

import com.google.common.base.Joiner;
import edu.unimib.biblioteca.exception.ResourceNotFoundException;
import edu.unimib.biblioteca.model.CasaEditrice;
import edu.unimib.biblioteca.repository.CasaEditriceRepository;
import edu.unimib.biblioteca.specification.EntitySpecificationsBuilder;
import edu.unimib.biblioteca.specification.SearchOperation;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CasaEditriceController {

  @Autowired private CasaEditriceRepository casaEditriceRepository;

  @GetMapping("/caseEditrici")
  public List<CasaEditrice> getCasaEditrici(
      @RequestParam(value = "search", required = false) String search) {
    if (search != null) {
      EntitySpecificationsBuilder<CasaEditrice> builder =
          new EntitySpecificationsBuilder<CasaEditrice>();
      String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
      // Viene creato un oggetto pattern che si occupa di interpretare la ricerca in input
      Pattern pattern =
          Pattern.compile(
              "(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)((\\w| )+?)(\\p{Punct}?),");
      // Viene interpretato l'input attraverso l'utilizzo del pattern
      Matcher matcher = pattern.matcher(search + ",");
      while (matcher.find()) {
        builder.with(
            matcher.group(1),
            matcher.group(2),
            matcher.group(4),
            matcher.group(3),
            matcher.group(6));
      }
      // Viene creata una specification che sarà utilizzata per filtrare il dataset.
      Specification<CasaEditrice> spec = builder.build();
      return casaEditriceRepository.findAll(spec);
    } else {
      return casaEditriceRepository.findAll();
    }
  }

  @PostMapping("/caseEditrici")
  public CasaEditrice createCasaEditrice(@Valid @RequestBody CasaEditrice casaEditriceRequest) {
    return casaEditriceRepository.save(casaEditriceRequest);
  }

  @GetMapping("/caseEditrici/{casaEditriceId}")
  public CasaEditrice getCasaEditrice(@PathVariable Long casaEditriceId) {
    return casaEditriceRepository
        .findById(casaEditriceId)
        .orElseThrow(
            () ->
                new ResourceNotFoundException("CasaEditrice not found with id " + casaEditriceId));
  }

  @PostMapping("/caseEditrici/{casaEditriceId}")
  public CasaEditrice updateCasaEditrice(
      @PathVariable Long casaEditriceId, @Valid @RequestBody CasaEditrice casaEditriceRequest) {
    return casaEditriceRepository
        .findById(casaEditriceId)
        .map(
            casaEditrice -> {
              casaEditrice.setLibri(casaEditriceRequest.getLibri());
              casaEditrice.setNome(casaEditriceRequest.getNome());
              return casaEditriceRepository.save(casaEditrice);
            })
        .orElseThrow(
            () ->
                new ResourceNotFoundException("CasaEditrice not found with id " + casaEditriceId));
  }

  @DeleteMapping("/caseEditrici/{casaEditriceId}")
  public ResponseEntity<?> deleteCasaEditrice(@PathVariable Long casaEditriceId) {
    return casaEditriceRepository
        .findById(casaEditriceId)
        .map(
            casaEditrice -> {
              casaEditriceRepository.delete(casaEditrice);
              return ResponseEntity.ok().build();
            })
        .orElseThrow(
            () ->
                new ResourceNotFoundException("CasaEditrice not found with id " + casaEditriceId));
  }
}
