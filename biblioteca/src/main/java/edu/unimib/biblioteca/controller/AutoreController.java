package edu.unimib.biblioteca.controller;

import com.google.common.base.Joiner;
import edu.unimib.biblioteca.exception.ResourceNotFoundException;
import edu.unimib.biblioteca.model.Autore;
import edu.unimib.biblioteca.model.Libro;
import edu.unimib.biblioteca.repository.AutoreRepository;
import edu.unimib.biblioteca.repository.LibroRepository;
import edu.unimib.biblioteca.specification.EntitySpecificationsBuilder;
import edu.unimib.biblioteca.specification.SearchOperation;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AutoreController {

  @Autowired private AutoreRepository autoreRepository;

  @Autowired private LibroRepository libroRepository;

  @GetMapping("/autori")
  public List<Autore> getAutori(@RequestParam(value = "search", required = false) String search) {
    if (search != null) {
      EntitySpecificationsBuilder<Autore> builder = new EntitySpecificationsBuilder<Autore>();
      String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
      // Viene creato un oggetto pattern che si occupa di interpretare la ricerca in input
      Pattern pattern =
          Pattern.compile(
              "(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)((\\w| )+?)(\\p{Punct}?),");
      // Viene interpretato l'input attraverso l'utilizzo del pattern
      Matcher matcher = pattern.matcher(search + ",");
      while (matcher.find()) {
        builder.with(
            matcher.group(1),
            matcher.group(2),
            matcher.group(4),
            matcher.group(3),
            matcher.group(6));
      }
      // Viene creata una specification che sarà utilizzata per filtrare il dataset.
      Specification<Autore> spec = builder.build();
      return autoreRepository.findAll(spec);
    } else {
      return autoreRepository.findAll();
    }
  }

  @PostMapping("/autori")
  public Autore createAutore(@Valid @RequestBody Autore autoreRequest) {
    return autoreRepository.save(autoreRequest);
  }

  @GetMapping("/autori/{autoreId}")
  public Autore getAutore(@PathVariable Long autoreId) {
    return autoreRepository
        .findById(autoreId)
        .orElseThrow(() -> new ResourceNotFoundException("Autore not found with id " + autoreId));
  }

  @PostMapping("/autori/{autoreId}")
  public Autore updateAutore(
      @PathVariable Long autoreId, @Valid @RequestBody Autore autoreRequest) {
    return autoreRepository
        .findById(autoreId)
        .map(
            autore -> {
              autore.setNome(autoreRequest.getNome());
              autore.setCognome(autoreRequest.getCognome());
              return autoreRepository.save(autore);
            })
        .orElseThrow(() -> new ResourceNotFoundException("Autore not found with id " + autoreId));
  }

  @DeleteMapping("/autori/{autoreId}")
  public ResponseEntity<?> deleteAutore(@PathVariable Long autoreId) {
    return autoreRepository
        .findById(autoreId)
        .map(
            autore -> {
              if (autore.getLibri().size() > 0) {
                for (Libro libro : autore.getLibri()) {
                  libro.getAutori().remove(autore);

                  // Rimuoviamo tutti i libri in cui l'autore è l'unico scrittore
                  if (libro.getAutori().size() == 0) {
                    Libro tmpLibro = libroRepository.findByPrequel(libro);
                    if (tmpLibro != null) tmpLibro.setPrequel(null);
                    libroRepository.delete(libro);
                  }
                }
              }
              autoreRepository.delete(autore);
              return ResponseEntity.ok().build();
            })
        .orElseThrow(() -> new ResourceNotFoundException("Autore not found with id " + autoreId));
  }
}
