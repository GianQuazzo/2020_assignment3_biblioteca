package edu.unimib.biblioteca.controller;

import com.google.common.base.Joiner;
import edu.unimib.biblioteca.exception.ResourceNotFoundException;
import edu.unimib.biblioteca.model.ClienteNonTesserato;
import edu.unimib.biblioteca.model.Copia;
import edu.unimib.biblioteca.repository.ClienteNonTesseratoRepository;
import edu.unimib.biblioteca.specification.EntitySpecificationsBuilder;
import edu.unimib.biblioteca.specification.SearchOperation;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteNonTesseratoController {
  @Autowired private ClienteNonTesseratoRepository clienteNonTesseratoRepository;

  @GetMapping("/clientiNonTesserati")
  public List<ClienteNonTesserato> getClientiNonTesserati(
      @RequestParam(value = "search", required = false) String search) {
    if (search != null) {
      EntitySpecificationsBuilder<ClienteNonTesserato> builder =
          new EntitySpecificationsBuilder<ClienteNonTesserato>();
      String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
      // Viene creato un oggetto pattern che si occupa di interpretare la ricerca in input
      Pattern pattern =
          Pattern.compile(
              "(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)((\\w| )+?)(\\p{Punct}?),");
      // Viene interpretato l'input attraverso l'utilizzo del pattern
      Matcher matcher = pattern.matcher(search + ",");
      while (matcher.find()) {
        builder.with(
            matcher.group(1),
            matcher.group(2),
            matcher.group(4),
            matcher.group(3),
            matcher.group(6));
      }
      // Viene creata una specification che sarà utilizzata per filtrare il dataset.
      Specification<ClienteNonTesserato> spec = builder.build();
      return clienteNonTesseratoRepository.findAll(spec);
    } else {
      return clienteNonTesseratoRepository.findAll();
    }
  }

  @PostMapping("/clientiNonTesserati")
  public ClienteNonTesserato createClienteNonTesserato(
      @Valid @RequestBody ClienteNonTesserato clienteNonTesserato) {
    return clienteNonTesseratoRepository.save(clienteNonTesserato);
  }

  @GetMapping("/clientiNonTesserati/{clienteNonTesseratoId}")
  public ClienteNonTesserato getClienteNonTesserato(@PathVariable Long clienteNonTesseratoId) {

    return clienteNonTesseratoRepository
        .findById(clienteNonTesseratoId)
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    "ClienteNonTesserato not found with id " + clienteNonTesseratoId));
  }

  @PostMapping("/clientiNonTesserati/{clienteNonTesseratoId}")
  public ClienteNonTesserato updateClienteNonTesserato(
      @PathVariable Long clienteNonTesseratoId,
      @Valid @RequestBody ClienteNonTesserato clienteNonTesseratoRequest) {
    return clienteNonTesseratoRepository
        .findById(clienteNonTesseratoId)
        .map(
            clienteNonTesserato -> {
              clienteNonTesserato.setCodiceFiscale(clienteNonTesseratoRequest.getCodiceFiscale());
              clienteNonTesserato.setNome(clienteNonTesseratoRequest.getNome());
              clienteNonTesserato.setCognome(clienteNonTesseratoRequest.getCognome());
              return clienteNonTesseratoRepository.save(clienteNonTesserato);
            })
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    "ClienteNonTesserato not found with id " + clienteNonTesseratoId));
  }

  @DeleteMapping("/clientiNonTesserati/{clienteNonTesseratoId}")
  public ResponseEntity<?> deleteClienteNonTesserato(@PathVariable Long clienteNonTesseratoId) {
    return clienteNonTesseratoRepository
        .findById(clienteNonTesseratoId)
        .map(
            clienteNonTesserato -> {
              // prima di eliminare il cliente bisogna rilasciare le copie a lui assegnate
              Copia copia =
                  clienteNonTesseratoRepository.findById(clienteNonTesseratoId).get().getCopia();
              if (copia != null) {
                copia.setDisponibile(true);
                copia.setCliente(null);
              }

              clienteNonTesseratoRepository.delete(clienteNonTesserato);
              return ResponseEntity.ok().build();
            })
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    "ClienteNonTesserato not found with id " + clienteNonTesseratoId));
  }
}
