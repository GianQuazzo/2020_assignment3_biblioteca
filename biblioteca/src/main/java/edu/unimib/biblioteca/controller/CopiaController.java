package edu.unimib.biblioteca.controller;

import com.google.common.base.Joiner;
import edu.unimib.biblioteca.exception.ResourceNotFoundException;
import edu.unimib.biblioteca.model.Cliente;
import edu.unimib.biblioteca.model.ClienteNonTesserato;
import edu.unimib.biblioteca.model.Copia;
import edu.unimib.biblioteca.model.Libro;
import edu.unimib.biblioteca.repository.ClienteNonTesseratoRepository;
import edu.unimib.biblioteca.repository.ClienteRepository;
import edu.unimib.biblioteca.repository.CopiaRepository;
import edu.unimib.biblioteca.repository.LibroRepository;
import edu.unimib.biblioteca.specification.EntitySpecificationsBuilder;
import edu.unimib.biblioteca.specification.SearchOperation;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class CopiaController {
  @Autowired private CopiaRepository copiaRepository;
  @Autowired private LibroRepository libroRepository;
  @Autowired private ClienteRepository clienteRepository;
  @Autowired private ClienteNonTesseratoRepository clienteNonTesseratoRepository;

  @GetMapping("/copie")
  public List<Copia> getCopie(@RequestParam(value = "search", required = false) String search) {
    if (search != null) {
      EntitySpecificationsBuilder<Copia> builder = new EntitySpecificationsBuilder<Copia>();
      String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
      // Viene creato un oggetto pattern che si occupa di interpretare la ricerca in input
      Pattern pattern =
          Pattern.compile(
              "(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)((\\w| )+?)(\\p{Punct}?),");
      // Viene interpretato l'input attraverso l'utilizzo del pattern
      Matcher matcher = pattern.matcher(search + ",");
      while (matcher.find()) {
        builder.with(
            matcher.group(1),
            matcher.group(2),
            matcher.group(4),
            matcher.group(3),
            matcher.group(6));
      }
      // Viene creata una specification che sarà utilizzata per filtrare il dataset.
      Specification<Copia> spec = builder.build();
      return copiaRepository.findAll(spec);
    } else {
      return copiaRepository.findAll();
    }
  }

  @PostMapping("/copie")
  public Copia createCopia(@Valid @RequestBody Copia copiaRequest) {
    if ((copiaRequest.isDisponibile() && copiaRequest.getCliente() != null)
        || (!copiaRequest.isDisponibile() && copiaRequest.getCliente() == null))
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          "Non puoi creare una copia disponibile con un cliente assegnatole o una non disponibile"
              + " senza cliente");
    if (copiaRequest.getCliente() != null) {
      ClienteNonTesserato clienteNonTesserato =
          clienteNonTesseratoRepository.findById(copiaRequest.getCliente().getId()).orElse(null);
      if (clienteNonTesserato != null && clienteNonTesserato.getCopia() != null)
        throw new ResponseStatusException(
            HttpStatus.BAD_REQUEST, "Un cliente non tesserato può avere solo una copia alla volta");
    }
    return copiaRepository.save(copiaRequest);
  }

  @GetMapping("/copie/{copiaId}")
  public Copia getCopia(@PathVariable Long copiaId) {
    return copiaRepository
        .findById(copiaId)
        .orElseThrow(() -> new ResourceNotFoundException("Copia not found with id " + copiaId));
  }

  @PostMapping("/copie/{copiaId}")
  public Copia updateCopia(@PathVariable Long copiaId, @Valid @RequestBody Copia copiaRequest) {

    return copiaRepository
        .findById(copiaId)
        .map(
            copia -> {
              if ((copiaRequest.isDisponibile() && copiaRequest.getCliente() != null)
                  || (!copiaRequest.isDisponibile() && copiaRequest.getCliente() == null))
                throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Non puoi avere una copia disponibile con un cliente assegnatole o una non"
                        + " disponibile senza cliente");
              if (copiaRequest.getCliente() != null) {
                ClienteNonTesserato clienteNonTesserato =
                    clienteNonTesseratoRepository
                        .findById(copiaRequest.getCliente().getId())
                        .orElse(null);
                if (clienteNonTesserato != null
                    && clienteNonTesserato.getCopia() != null
                    && clienteNonTesserato.getCopia().getId() != copiaId)
                  throw new ResponseStatusException(
                      HttpStatus.BAD_REQUEST,
                      "Un cliente non tesserato può avere solo una copia alla volta");
              }
              copia.setDisponibile(copiaRequest.isDisponibile());
              copia.setCliente(copiaRequest.getCliente());
              copia.setLibro(copiaRequest.getLibro());
              return copiaRepository.save(copia);
            })
        .orElseThrow(() -> new ResourceNotFoundException("Copia not found with id " + copiaId));
  }

  @DeleteMapping("/copie/{copiaId}")
  public ResponseEntity<?> deleteCopia(@PathVariable Long copiaId) {
    return copiaRepository
        .findById(copiaId)
        .map(
            copia -> {
              copiaRepository.delete(copia);
              return ResponseEntity.ok().build();
            })
        .orElseThrow(() -> new ResourceNotFoundException("Copia not found with id " + copiaId));
  }
  
  @GetMapping("/copie/byLibroTitolo/{libroTitolo}")
  public List<Copia> getCopieByLibroTitolo(@PathVariable String libroTitolo){
	  List<Libro> libri = libroRepository.findLibriByTitolo(libroTitolo);  
	  if(libri == null || libri.size() == 0)
			throw new ResourceNotFoundException("Libro not found with titolo " + libroTitolo);
	  List<Copia> copie = copiaRepository.findAll();
	  List<Copia> copieLibriTitolo = new ArrayList<Copia>();
	  for(Copia copia : copie)
		  if(copia.getLibro() != null && libri.contains(copia.getLibro()))
			  copieLibriTitolo.add(copia);
	  return copieLibriTitolo;
  } 
  
  @GetMapping("/copie/byCodiceFiscaleCliente/{codiceFiscaleCliente}")
  public List<Copia> getCopieByCodiceFiscaleCliente(@PathVariable String codiceFiscaleCliente){
	  Cliente cliente = clienteRepository.findClienteByCodiceFiscale(codiceFiscaleCliente);
	  if(cliente == null)
			throw new ResourceNotFoundException("Cliente not found with codiceFiscale " + codiceFiscaleCliente);
	  List<Copia> copie = copiaRepository.findCopieByCliente(cliente);
	  return copie;
  }
}
