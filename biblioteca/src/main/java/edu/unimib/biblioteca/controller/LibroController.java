package edu.unimib.biblioteca.controller;

import com.google.common.base.Joiner;
import edu.unimib.biblioteca.exception.ResourceNotFoundException;
import edu.unimib.biblioteca.model.Autore;
import edu.unimib.biblioteca.model.CasaEditrice;
import edu.unimib.biblioteca.model.Libro;
import edu.unimib.biblioteca.repository.AutoreRepository;
import edu.unimib.biblioteca.repository.CasaEditriceRepository;
import edu.unimib.biblioteca.repository.LibroRepository;
import edu.unimib.biblioteca.specification.EntitySpecificationsBuilder;
import edu.unimib.biblioteca.specification.SearchOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LibroController {

  @Autowired private AutoreRepository autoreRepository;
  @Autowired private CasaEditriceRepository casaEditriceRepository;
  @Autowired private LibroRepository libroRepository;

  @GetMapping("/libri")
  public List<Libro> getLibri(@RequestParam(value = "search", required = false) String search) {
    if (search != null) {
      EntitySpecificationsBuilder<Libro> builder = new EntitySpecificationsBuilder<Libro>();
      String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
      // Viene creato un oggetto pattern che si occupa di interpretare la ricerca in input
      Pattern pattern =
          Pattern.compile(
              "(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)((\\w| )+?)(\\p{Punct}?),");
      // Viene interpretato l'input attraverso l'utilizzo del pattern
      Matcher matcher = pattern.matcher(search + ",");
      while (matcher.find()) {
        builder.with(
            matcher.group(1),
            matcher.group(2),
            matcher.group(4),
            matcher.group(3),
            matcher.group(6));
      }
      // Viene creata una specification che sarà utilizzata per filtrare il dataset.
      Specification<Libro> spec = builder.build();
      return libroRepository.findAll(spec);
    } else {
      return libroRepository.findAll();
    }
  }

  @PostMapping("/libri")
  public Libro createLibro(@Valid @RequestBody Libro libroRequest) {
    return libroRepository.save(libroRequest);
  }

  @GetMapping("/libri/{libroId}")
  public Libro getLibro(@PathVariable Long libroId) {
    return libroRepository
        .findById(libroId)
        .orElseThrow(() -> new ResourceNotFoundException("Libro not found with id " + libroId));
  }

  @PostMapping("/libri/{libroId}")
  public Libro updateLibro(@PathVariable Long libroId, @Valid @RequestBody Libro libroRequest) {
    return libroRepository
        .findById(libroId)
        .map(
            libro -> {
              libro.setAnno(libroRequest.getAnno());
              libro.setCasaEditrice(libroRequest.getCasaEditrice());
              libro.setIsbn(libroRequest.getIsbn());
              libro.setTitolo(libroRequest.getTitolo());
              libro.setAutori(libroRequest.getAutori());
              libro.setPrequel(libroRequest.getPrequel());
              return libroRepository.save(libro);
            })
        .orElseThrow(() -> new ResourceNotFoundException("Libro not found with id " + libroId));
  }

  @DeleteMapping("/libri/{libroId}")
  public ResponseEntity<?> delateLibro(@PathVariable Long libroId) {
    return libroRepository
        .findById(libroId)
        .map(
            libro -> {
              Libro libroSequel = libroRepository.findByPrequel(libro);
              if (libroSequel != null) libroSequel.setPrequel(null);
              libroRepository.delete(libro);
              return ResponseEntity.ok().build();
            })
        .orElseThrow(() -> new ResourceNotFoundException("Libro not found with id " + libroId));
  }
  
  @GetMapping("/libri/byNomeCasaEditrice/{nomeCasaEditrice}")
  public List<Libro> getLibriByCasaEditrice(@PathVariable String nomeCasaEditrice){
	  CasaEditrice casaEditrice = casaEditriceRepository.findByNome(nomeCasaEditrice);
	  if(casaEditrice == null) {
		throw new ResourceNotFoundException("CasaEditrice not found with nome " + nomeCasaEditrice);
	  }
      return libroRepository.findLibriByCasaEditrice(casaEditrice);
  }
 
  @GetMapping("/libri/byNomeAutore/{nomeAutore}")
  public List<Libro> getLibriByNomeAutore(@PathVariable String nomeAutore){
	  List<Autore> autori = autoreRepository.findAutoriByNome(nomeAutore);
	  if(autori == null || autori.size() == 0)
			throw new ResourceNotFoundException("Autore not found with nome " + nomeAutore);
	  List<Libro> libri = libroRepository.findAll();
	  List<Libro> libriAutore = new ArrayList<Libro>();
	  for(Libro libro : libri)
		  for(Autore autore : autori)
			  if(libro.getAutori().contains(autore) && !libriAutore.contains(autore))
				  libriAutore.add(libro);
	  return libriAutore;
  }
}
