package edu.unimib.biblioteca.controller;

import com.google.common.base.Joiner;
import edu.unimib.biblioteca.exception.ResourceNotFoundException;
import edu.unimib.biblioteca.model.ClienteTesserato;
import edu.unimib.biblioteca.model.Copia;
import edu.unimib.biblioteca.repository.ClienteTesseratoRepository;
import edu.unimib.biblioteca.specification.EntitySpecificationsBuilder;
import edu.unimib.biblioteca.specification.SearchOperation;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteTesseratoController {
  @Autowired private ClienteTesseratoRepository clienteTesseratoRepository;

  @GetMapping("/clientiTesserati")
  public List<ClienteTesserato> getClientiTesserati(
      @RequestParam(value = "search", required = false) String search) {
    if (search != null) {
      EntitySpecificationsBuilder<ClienteTesserato> builder =
          new EntitySpecificationsBuilder<ClienteTesserato>();
      String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);
      // Viene creato un oggetto pattern che si occupa di interpretare la ricerca in input
      Pattern pattern =
          Pattern.compile(
              "(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)((\\w| )+?)(\\p{Punct}?),");
      // Viene interpretato l'input attraverso l'utilizzo del pattern
      Matcher matcher = pattern.matcher(search + ",");
      while (matcher.find()) {
        builder.with(
            matcher.group(1),
            matcher.group(2),
            matcher.group(4),
            matcher.group(3),
            matcher.group(6));
      }
      // Viene creata una specification che sarà utilizzata per filtrare il dataset.
      Specification<ClienteTesserato> spec = builder.build();
      return clienteTesseratoRepository.findAll(spec);
    } else {
      return clienteTesseratoRepository.findAll();
    }
  }

  @PostMapping("/clientiTesserati")
  public ClienteTesserato createClienteTesserato(
      @Valid @RequestBody ClienteTesserato clienteTesserato) {
    return clienteTesseratoRepository.save(clienteTesserato);
  }

  @GetMapping("/clientiTesserati/{clienteTesseratoId}")
  public ClienteTesserato getClienteTesserato(@PathVariable Long clienteTesseratoId) {

    return clienteTesseratoRepository
        .findById(clienteTesseratoId)
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    "ClienteTesserato not found with id " + clienteTesseratoId));
  }

  @PostMapping("/clientiTesserati/{clienteTesseratoId}")
  public ClienteTesserato updateClienteTesserato(
      @PathVariable Long clienteTesseratoId,
      @Valid @RequestBody ClienteTesserato clienteTesseratoRequest) {
    return clienteTesseratoRepository
        .findById(clienteTesseratoId)
        .map(
            clienteTesserato -> {
              clienteTesserato.setCodiceFiscale(clienteTesseratoRequest.getCodiceFiscale());
              clienteTesserato.setNome(clienteTesseratoRequest.getNome());
              clienteTesserato.setCognome(clienteTesseratoRequest.getCognome());
              clienteTesserato.setIdTessera(clienteTesseratoRequest.getIdTessera());
              return clienteTesseratoRepository.save(clienteTesserato);
            })
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    "ClienteTesserato not found with id " + clienteTesseratoId));
  }

  @DeleteMapping("/clientiTesserati/{clienteTesseratoId}")
  public ResponseEntity<?> deleteClienteTesserato(@PathVariable Long clienteTesseratoId) {
    return clienteTesseratoRepository
        .findById(clienteTesseratoId)
        .map(
            clienteTesserato -> {
              // prima di eliminare il cliente bisogna rilasciare le copie a lui assegnate
              Set<Copia> listaCopie =
                  clienteTesseratoRepository.findById(clienteTesseratoId).get().getCopie();
              for (Copia copia : listaCopie) {
                copia.setDisponibile(true);
                copia.setCliente(null);
              }

              clienteTesseratoRepository.delete(clienteTesserato);
              return ResponseEntity.ok().build();
            })
        .orElseThrow(
            () ->
                new ResourceNotFoundException(
                    "ClienteTesserato not found with id " + clienteTesseratoId));
  }
}
