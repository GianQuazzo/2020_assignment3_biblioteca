package edu.unimib.biblioteca.specification;
/**
 * La classe SearchOperation contiene una mappa degli operatori logici che possono essere utilizzati
 * per creare espressioni di ricerca.
 */
public enum SearchOperation {
  EQUALITY,
  NEGATION,
  GREATER_THAN,
  LESS_THAN,
  LIKE,
  STARTS_WITH,
  ENDS_WITH,
  CONTAINS;

  public static final String[] SIMPLE_OPERATION_SET = {":", "!", ">", "<", "~"};

  public static SearchOperation getSimpleOperation(char input) {
    switch (input) {
      case ':':
        return EQUALITY;
      case '!':
        return NEGATION;
      case '>':
        return GREATER_THAN;
      case '<':
        return LESS_THAN;
      case '~':
        return LIKE;
      default:
        return null;
    }
  }
}
