package edu.unimib.biblioteca.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class EntitySpecification<T> implements Specification<T> {

  private SearchCriteria criteria;

  public EntitySpecification(final SearchCriteria criteria) {
    super();
    this.criteria = criteria;
  }

  public SearchCriteria getCriteria() {
    return criteria;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

    switch (criteria.getOperation()) {
      case EQUALITY:
        if (criteria.getValue().toString().equalsIgnoreCase("true")
            || criteria.getValue().toString().equalsIgnoreCase("false"))
          return builder.equal(
              root.get(criteria.getKey()), Boolean.valueOf(criteria.getValue().toString()));
        else if (criteria.getValue().toString().equalsIgnoreCase("null"))
          return builder.isNull(root.get(criteria.getKey()));
        else return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      case NEGATION:
        if (criteria.getValue().toString().equalsIgnoreCase("true")
            || criteria.getValue().toString().equalsIgnoreCase("false"))
          return builder.notEqual(
              root.get(criteria.getKey()), Boolean.valueOf(criteria.getValue().toString()));
        else if (criteria.getValue().toString().equalsIgnoreCase("null"))
          return builder.isNotNull((root.get(criteria.getKey())));
        else return builder.notEqual(root.get(criteria.getKey()), criteria.getValue());
      case GREATER_THAN:
        return builder.greaterThan(
            root.<String>get(criteria.getKey()), criteria.getValue().toString());
      case LESS_THAN:
        return builder.lessThan(
            root.<String>get(criteria.getKey()), criteria.getValue().toString());
      case LIKE:
        return builder.like(root.<String>get(criteria.getKey()), criteria.getValue().toString());
      case STARTS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), criteria.getValue() + "%");
      case ENDS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue());
      case CONTAINS:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      default:
        return null;
    }
  }
}
