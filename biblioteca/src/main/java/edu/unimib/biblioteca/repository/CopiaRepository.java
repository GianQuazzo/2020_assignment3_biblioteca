package edu.unimib.biblioteca.repository;

import edu.unimib.biblioteca.model.Cliente;
import edu.unimib.biblioteca.model.Copia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CopiaRepository
    extends JpaRepository<Copia, Long>, JpaSpecificationExecutor<Copia> {
	List<Copia> findCopieByCliente(Cliente cliente);
}