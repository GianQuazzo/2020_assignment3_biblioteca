package edu.unimib.biblioteca.repository;

import edu.unimib.biblioteca.model.ClienteNonTesserato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteNonTesseratoRepository
    extends JpaRepository<ClienteNonTesserato, Long>,
        JpaSpecificationExecutor<ClienteNonTesserato> {}
