package edu.unimib.biblioteca.repository;

import edu.unimib.biblioteca.model.ClienteTesserato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteTesseratoRepository
    extends JpaRepository<ClienteTesserato, Long>, JpaSpecificationExecutor<ClienteTesserato> {}
