package edu.unimib.biblioteca.repository;

import edu.unimib.biblioteca.model.CasaEditrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CasaEditriceRepository
    extends JpaRepository<CasaEditrice, Long>, JpaSpecificationExecutor<CasaEditrice> {
	CasaEditrice findByNome(String nome);
}