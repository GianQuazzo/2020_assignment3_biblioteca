package edu.unimib.biblioteca.repository;

import edu.unimib.biblioteca.model.Autore;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AutoreRepository
    extends JpaRepository<Autore, Long>, JpaSpecificationExecutor<Autore> {
	List<Autore> findAutoriByNome(String nome);
}
