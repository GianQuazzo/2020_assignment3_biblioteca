package edu.unimib.biblioteca.repository;

import edu.unimib.biblioteca.model.CasaEditrice;
import edu.unimib.biblioteca.model.Libro;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LibroRepository
    extends JpaRepository<Libro, Long>, JpaSpecificationExecutor<Libro> {
  Libro findByPrequel(Libro prequel);
  List<Libro> findLibriByCasaEditrice(CasaEditrice casaEditrice);
  List<Libro> findLibriByTitolo(String titolo);
}