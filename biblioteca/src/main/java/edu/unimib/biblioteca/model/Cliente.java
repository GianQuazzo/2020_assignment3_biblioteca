package edu.unimib.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "cliente")
@Table
@Inheritance(strategy = InheritanceType.JOINED)
public class Cliente {
  @Id
  @GeneratedValue(generator = "cliente_generator")
  @SequenceGenerator(
      name = "cliente_generator",
      sequenceName = "cliente_sequence",
      initialValue = 1000)
  private long id;

  @Column(name = "nome")
  @NotNull
  private String nome;

  @Column(name = "cognome")
  @NotNull
  private String cognome;

  @Column(name = "codiceFiscale")
  @Size(min = 16, max = 16)
  @NotNull
  private String codiceFiscale;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCognome() {
    return cognome;
  }

  public void setCognome(String cognome) {
    this.cognome = cognome;
  }

  public String getCodiceFiscale() {
    return codiceFiscale;
  }

  public void setCodiceFiscale(String codiceFiscale) {
    this.codiceFiscale = codiceFiscale;
  }
}
