package edu.unimib.biblioteca.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity(name = "libro")
@Table
public class Libro {
  @Id
  @GeneratedValue(generator = "libro_generator")
  @SequenceGenerator(name = "libro_generator", sequenceName = "libro_sequence", initialValue = 1000)
  private long id;

  @Column(name = "isbn")
  @NotNull
  private Long isbn;

  @Column(name = "titolo")
  @NotNull
  private String titolo;

  @Column(name = "anno")
  @NotNull
  private int anno;

  @ManyToOne
  @JoinColumn(name = "casaEditrice_id")
  @NotNull
  private CasaEditrice casaEditrice;

  @ManyToMany
  @JoinColumn(name = "autori_id")
  @NotEmpty
  private Set<Autore> autori = new HashSet<Autore>();

  @OneToMany(mappedBy = "libro", cascade = CascadeType.REMOVE)
  @JsonIgnore
  private Set<Copia> copie = new HashSet<Copia>();

  @OneToOne private Libro prequel;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Long getIsbn() {
    return isbn;
  }

  public void setIsbn(Long isbn) {
    this.isbn = isbn;
  }

  public String getTitolo() {
    return titolo;
  }

  public void setTitolo(String titolo) {
    this.titolo = titolo;
  }

  public int getAnno() {
    return anno;
  }

  public void setAnno(int anno) {
    this.anno = anno;
  }

  public CasaEditrice getCasaEditrice() {
    return casaEditrice;
  }

  public void setCasaEditrice(CasaEditrice casaEditrice) {
    this.casaEditrice = casaEditrice;
  }

  public Set<Autore> getAutori() {
    return autori;
  }

  public void setAutori(Set<Autore> autori) {
    this.autori = autori;
  }

  public Set<Copia> getCopie() {
    return copie;
  }

  public void setCopie(Set<Copia> copie) {
    this.copie = copie;
  }

  public Libro getPrequel() {
    return prequel;
  }

  public void setPrequel(Libro prequel) {
    this.prequel = prequel;
  }
}
