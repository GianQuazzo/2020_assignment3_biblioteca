package edu.unimib.biblioteca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "copia")
@Table
public class Copia {
  @Id
  @GeneratedValue(generator = "copia_generator")
  @SequenceGenerator(name = "copia_generator", sequenceName = "copia_sequence", initialValue = 1000)
  private long id;

  @ManyToOne
  @JoinColumn(name = "libro_id")
  @NotNull
  private Libro libro;

  @Column(name = "disponibile")
  @NotNull
  private boolean disponibile;

  @ManyToOne
  @JoinColumn(name = "cliente_id")
  private Cliente cliente;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Libro getLibro() {
    return libro;
  }

  public void setLibro(Libro libro) {
    this.libro = libro;
  }

  public boolean isDisponibile() {
    return disponibile;
  }

  public void setDisponibile(boolean disponibile) {
    this.disponibile = disponibile;
  }

  public Cliente getCliente() {
    return cliente;
  }

  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }
}
