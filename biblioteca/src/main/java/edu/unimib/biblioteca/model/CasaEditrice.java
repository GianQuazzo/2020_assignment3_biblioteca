package edu.unimib.biblioteca.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "casa_editrice")
@Table
public class CasaEditrice {
  @Id
  @GeneratedValue(generator = "casa_editrice_generator")
  @SequenceGenerator(
      name = "casa_editrice_generator",
      sequenceName = "casa_editrice_sequence",
      initialValue = 1000)
  private long id;

  @Column(name = "nome")
  @NotNull
  private String nome;

  @OneToMany(mappedBy = "casaEditrice", cascade = CascadeType.REMOVE)
  @JsonIgnore
  private Set<Libro> libri = new HashSet<Libro>();

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Set<Libro> getLibri() {
    return libri;
  }

  public void setLibri(Set<Libro> libri) {
    this.libri = libri;
  }
}
