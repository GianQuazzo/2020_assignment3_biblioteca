package edu.unimib.biblioteca.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "autore")
@Table
public class Autore {
  @Id
  @GeneratedValue(generator = "autore_generator")
  @SequenceGenerator(
      name = "autore_generator",
      sequenceName = "autore_sequence",
      initialValue = 1000)
  private long id;

  @Column(name = "nome")
  @NotNull
  private String nome;

  @Column(name = "cognome")
  @NotNull
  private String cognome;

  @ManyToMany(mappedBy = "autori", cascade = CascadeType.DETACH)
  @JsonIgnore
  private Set<Libro> libri = new HashSet<Libro>();

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCognome() {
    return cognome;
  }

  public void setCognome(String cognome) {
    this.cognome = cognome;
  }

  public Set<Libro> getLibri() {
    return libri;
  }

  public void setLibri(Set<Libro> libri) {
    this.libri = libri;
  }
}
