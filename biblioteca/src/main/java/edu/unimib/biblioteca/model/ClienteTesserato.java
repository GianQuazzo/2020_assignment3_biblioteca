package edu.unimib.biblioteca.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity(name = "cliente_tesserato")
@Inheritance(strategy = InheritanceType.JOINED)
public class ClienteTesserato extends Cliente {

  @Column(name = "idTessera")
  @NotNull
  private Long idTessera;

  @OneToMany(mappedBy = "cliente")
  @JsonIgnore
  private Set<Copia> copie = new HashSet<Copia>();

  public Long getIdTessera() {
    return idTessera;
  }

  public void setIdTessera(Long idTessera) {
    this.idTessera = idTessera;
  }

  public Set<Copia> getCopie() {
    return copie;
  }
}
