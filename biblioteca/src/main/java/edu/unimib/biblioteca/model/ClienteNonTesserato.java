package edu.unimib.biblioteca.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity(name = "cliente_non_tesserato")
@Inheritance(strategy = InheritanceType.JOINED)
public class ClienteNonTesserato extends Cliente {

  @OneToOne(mappedBy = "cliente", fetch = FetchType.LAZY)
  @JsonIgnore
  private Copia copia;

  public Copia getCopia() {
    return copia;
  }

  public void setCopia(Copia copia) {
    this.copia = copia;
  }
}
