# Biblioteca

## Membri del gruppo
* Gianluca Quaglia 829533
* Michele Rago 830616

## Come eseguire

1. Modificare il file application.properties inserendo le informazioni relative al database (per facilitare la correzione dell' Assignament abbiamo reso disponibile la 
nostra istanza di PostgreSQL in rete, di conseguenza il passaggio appena descritto non risulta necessario per la correzione);
1. Creare il package eseguendo il comando
```bash
./mvnw clean package -DskipTests
```
3. Eserguire l'applicazione con il comando
```bash
java -jar target/biblioteca-0.0.1-SNAPSHOT.jar
```
L'applicazione sarà disponibile all'indirizzo [http://localhost/:8080](http://localhost:8080)
